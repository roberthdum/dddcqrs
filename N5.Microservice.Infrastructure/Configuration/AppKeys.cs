﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DDDCQRS.MyMicroservice.Api.Configuration
{
    public class AppKeys
    {
        public const string Bearer = "Bearer";

        public struct JwTokenn
        {
            public const string TokenExpirationError = "TokenExpirationError";
            public const string TokenAuth = "TokenAuth";
            public const string MissingData = "MissingData";
            public const string SymmetricSecurityKey = "symmetricSecurityKey";
            public const string TokenTime = "tokenTime";
        }

        public struct FileSettings
        {
            public const string ValidFileFormats = "validFileFormat";
            public const string ValidFileSize = "validFileSize";
            public const string Image = "image";
            public const string DocumentUploadToServer = "documentUploadToServer";
            public const string DocumentUploadSettings = "documentUploadSettings";
            public const string DocumentsPath = "documentUploadSettings:documentsPath";
            public const string DocumentRouteGetFile = "documentUploadSettings:routeGetFile";
            public const string DocumentUribasePath = "documentUploadSettings:uribasePath";
            public const string FileFormat = "base64";
        }
    }
}
